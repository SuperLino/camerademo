package com.gdkdemo.camera;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;

import com.google.android.glass.media.CameraManager;

public class CameraDemoActivity extends Activity {
	private final int IMAGE_CAPTURE_REQUEST_CODE = 007;

	private static final String TAG = "CameraDemo";

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_camerademo);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == IMAGE_CAPTURE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {

				Bundle extras = data.getExtras();
				if (extras != null) {
					String thumbnailFilePath = extras
							.getString(CameraManager.EXTRA_THUMBNAIL_FILE_PATH);
					Log.d(TAG, "thumbnailFilePath = " + thumbnailFilePath);

					String pictureFilePath = extras
							.getString(CameraManager.EXTRA_PICTURE_FILE_PATH);
					Log.d(TAG, "pictureFilePath = " + pictureFilePath);

				} else {
					Log.w(TAG, "The returned intent does not include extras.");
				}

			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_CAMERA || keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
			startCapture();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	// Tap triggers photo taking...
	private void startCapture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(intent, IMAGE_CAPTURE_REQUEST_CODE);
	}

}
